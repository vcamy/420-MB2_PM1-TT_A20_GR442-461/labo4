package vcamydeb.teccart.labo4;

        import androidx.appcompat.app.AlertDialog;
        import androidx.appcompat.app.AppCompatActivity;
        import androidx.core.app.ActivityCompat;

        import android.Manifest;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;

public class MainActivity<GPSLocationTracker> extends AppCompatActivity {


    private Button btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnLogin = (Button)findViewById(R.id.buttonLogin);


    }



    public void goLogIn(View view) {

        Intent i = new Intent(this, Login.class);
        startActivity(i);
    }
}