package vcamydeb.teccart.labo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class Releves extends AppCompatActivity {

    private String data;
    private int x;
    private String [] entree;
    private String cmd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_releves);

        cmd="2";


        //listview + arraylist
        ListView listView = (ListView)findViewById(R.id.listeReleves);
        ArrayList<releve> relevesArrayList = new ArrayList<releve>();

        //recup intent
        Bundle ex = getIntent().getExtras();
        if (ex != null) {

            //recup des données patients dans variable
            data = ex.getString("releves");

        }


        //split de chaque patient
        final String[] dataPoints = data.split("<br>");


        x=0;
        int y=1;
        //split des champs de la bdd
        while ( y <= dataPoints.length)
        {
            entree = dataPoints[x].split("//");
            relevesArrayList.add(new releve(entree[0], entree[1], entree[2],entree[3],entree[4],entree[5], entree[6], entree[7],entree[8],entree[9],entree[10],
                    entree[11]));


            x++;
            y++;

        }

        relevesArrayAdapter adapter = new relevesArrayAdapter(this,R.layout.layout,relevesArrayList);
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(Releves.this, Detail.class);

                i.putExtra("releve", dataPoints[position]);

                startActivity(i);



            }
        });
    }
}