package vcamydeb.teccart.labo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class menu extends AppCompatActivity {

    private String loginInfos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Bundle ex = getIntent().getExtras();
        if (ex != null) {

            //recup des données patients dans variable
            loginInfos = ex.getString("loginInfo");

            if(loginInfos.contains("//"))
            {
                Toast.makeText(this, "Connexion reussie", Toast.LENGTH_SHORT).show();
                setContentView(R.layout.activity_menu);
            }
            else
            {
                Toast.makeText(this, "Username / password invalides", Toast.LENGTH_SHORT).show();
            }

        }
        else{
            setContentView(R.layout.activity_menu);
        }

    }

    public void newPoint(View view) {
        Intent i = new Intent(this, insert.class);
        startActivity(i);

    }


    public void listeReleves(View view) {
        String cmd = "3";

        asynctask select = new asynctask(this);
        select.execute(cmd);
    }
}