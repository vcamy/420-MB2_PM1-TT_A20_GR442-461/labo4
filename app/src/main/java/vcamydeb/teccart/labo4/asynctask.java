package vcamydeb.teccart.labo4;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class asynctask extends AsyncTask {
    private Context c;
    private AlertDialog log;

    private String result;

    public asynctask(Context c) {
        this.c = c;
    }
    @Override
    protected void onPreExecute() {
        this.log = new AlertDialog.Builder(this.c).create();
        this.log.setTitle("Loading");
    }

    @Override
    protected String doInBackground(Object[] param)
    {

        switch ((String) param[0]) {
            case "1":
                String cibleLogin = "http://192.168.0.195/meteo/login.php";
                try {


                    URL url = new URL(cibleLogin);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestMethod("POST");

                    OutputStream outs = con.getOutputStream();
                    BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                    String msg = URLEncoder.encode("username", "utf-8") + "=" +
                            URLEncoder.encode((String) param[1], "utf8") + "&" +
                            URLEncoder.encode("password", "utf-8") + "=" +
                            URLEncoder.encode((String) param[2], "utf8");

                    bufw.write(msg);
                    bufw.flush();
                    bufw.close();
                    outs.close();

                    InputStream ins = con.getInputStream();
                    BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                    String line;
                    StringBuffer sbuff = new StringBuffer();

                    while ((line = bufr.readLine()) != null) {

                        sbuff.append(line);

                    }
                    Intent i = new Intent(c, menu.class);
                    result = sbuff.toString();

                        //INTENT avec resultat de la requete select pour le login en parametre
                        i.putExtra("loginInfo", result);
                        c.startActivity(i);





                } catch (Exception ex) {
                    return ex.getMessage();
                }


                break;

            case "2":
                String cibleInsert = "http://192.168.0.195/meteo/insert.php";
                try {

                    URL url = new URL(cibleInsert);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestMethod("POST");

                    OutputStream outs = con.getOutputStream();
                    BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                    String msg = URLEncoder.encode("nom", "utf-8") + "=" +
                            URLEncoder.encode((String) param[1], "utf8") +
                            "&" + URLEncoder.encode("jour", "utf-8") + "=" +
                            URLEncoder.encode((String) param[2], "utf8") +
                            "&" + URLEncoder.encode("heure", "utf-8") + "=" +
                            URLEncoder.encode((String) param[3], "utf8") +
                            "&" + URLEncoder.encode("longitude", "utf-8") + "=" +
                            URLEncoder.encode((String) param[4], "utf8") +
                            "&" + URLEncoder.encode("latitude", "utf-8") + "=" +
                            URLEncoder.encode((String) param[5], "utf8") +
                            "&" + URLEncoder.encode("ville", "utf-8") + "=" +
                            URLEncoder.encode((String) param[6], "utf8") +
                            "&" + URLEncoder.encode("province", "utf-8") + "=" +
                            URLEncoder.encode((String) param[7], "utf8") +
                            "&" + URLEncoder.encode("pays", "utf-8") + "=" +
                            URLEncoder.encode((String) param[8], "utf8") +
                            "&" + URLEncoder.encode("pression", "utf-8") + "=" +
                            URLEncoder.encode((String) param[9], "utf8") +
                            "&" + URLEncoder.encode("temperature", "utf-8") + "=" +
                            URLEncoder.encode((String) param[10], "utf8") +
                            "&" + URLEncoder.encode("humidite", "utf-8") + "=" +
                            URLEncoder.encode((String) param[11], "utf8");

                    bufw.write(msg);
                    bufw.flush();
                    bufw.close();
                    outs.close();

                    InputStream ins = con.getInputStream();
                    BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                    String line;
                    StringBuffer sbuff = new StringBuffer();

                    while ((line = bufr.readLine()) != null) {
                        sbuff.append(line);

                    }

                    Intent i = new Intent(c,menu.class);
                    c.startActivity(i);


                } catch (Exception ex) {
                    return ex.getMessage();
                }
                break;

            case "3":
                String cibleSelect = "http://192.168.0.195/meteo/select.php";
                try {

                    URL url = new URL(cibleSelect);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestMethod("POST");

                    OutputStream outs = con.getOutputStream();
                    BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));


                    bufw.flush();
                    bufw.close();
                    outs.close();

                    InputStream ins = con.getInputStream();
                    BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                    String line;
                    StringBuffer sbuff = new StringBuffer();

                    while ((line = bufr.readLine()) != null) {
                        sbuff.append(line);

                    }

                    Intent i = new Intent(c, Releves.class );
                    result = sbuff.toString();

                    //INTENT
                    i.putExtra("releves", result);
                    c.startActivity(i);


                }
                catch(Exception ex)
                {
                    return ex.getMessage();
                }
                break;


        }
        return result;
    }

    @Override
    protected void onPostExecute(Object o) {

//
    }
}
