package vcamydeb.teccart.labo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Login extends AppCompatActivity {

    private String cmd;
    private EditText usernameTxt;
    private EditText passwordTxt;
    private String username;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }


    public void loginAdmin(View view) {

        cmd= "1";
        usernameTxt = (EditText)findViewById(R.id.editTextUsername);
        passwordTxt = (EditText)findViewById(R.id.editTextPassword);
        username = usernameTxt.getText().toString();
        password = passwordTxt.getText().toString();

        asynctask login = new asynctask(this);
        login.execute(cmd, username, password);

    }
}