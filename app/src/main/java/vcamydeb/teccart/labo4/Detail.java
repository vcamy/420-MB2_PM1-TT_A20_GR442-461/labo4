package vcamydeb.teccart.labo4;

import androidx.appcompat.app.AppCompatActivity;
import android.graphics.Matrix;
import android.widget.ImageView;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Detail extends AppCompatActivity {

    private TextView nom,time,emplacement,longitude,latitude,temp,barometre, humiditeTxt;
    private float temperature, pression, humidite;
    private String nomSt, heureSt,jourSt,longitudeSt,latitudeSt,villeSt,paysSt,provinceSt,temperatureSt,pressionSt,humiditeSt;

    private String [] detail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Bundle ex = getIntent().getExtras();

        String releve="";

        if(ex!=null){
            releve = ex.getString("releve");
        }

        detail = releve.split("//");


        nom = (TextView)findViewById(R.id.textViewNom);
        emplacement=(TextView)findViewById(R.id.textViewEmplacement);
        time = (TextView)findViewById(R.id.textViewTime);
        longitude=(TextView)findViewById(R.id.textViewLongitude);
        latitude=(TextView)findViewById(R.id.textViewLatitude);
        temp=(TextView)findViewById(R.id.textViewTemp);
        barometre=(TextView)findViewById(R.id.textViewBaro);
        humiditeTxt=(TextView)findViewById(R.id.textViewHumidite);

        nomSt = detail[1];
        jourSt = detail[2];
        heureSt = detail[3];
        longitudeSt = detail[4];
        latitudeSt = detail[5];
        villeSt = detail[6];
        provinceSt = detail[7];
        paysSt = detail[8];
        pressionSt = detail[9];
        temperatureSt = detail[10];
        humiditeSt = detail[11];

        nom.setText(nomSt);
        time.setText("Jour: "+jourSt+" Heure: "+heureSt);
        emplacement.setText(paysSt+", "+provinceSt+", "+villeSt);
        longitude.setText(longitudeSt);
        latitude.setText(latitudeSt);

        temperature = Float.parseFloat(temperatureSt);
        pression = Float.parseFloat(pressionSt);
        humidite = Float.parseFloat(humiditeSt);
        temp.setText("Température: "+temperature+"°C");
        barometre.setText("Pression: "+pression+"hPa");
        humiditeTxt.setText("Taux d'humidité relative: "+humidite+"%");

        //display de la temperature dans le thermometre
        ProgressBar progressTemp = (ProgressBar)findViewById(R.id.progressBarTemp);
        progressTemp.setMax(35);
        progressTemp.setMin(-30);
        progressTemp.setProgress((int)temperature);

        //display du taux d'humidité
        ProgressBar progressHumidite = (ProgressBar)findViewById(R.id.progressBarHumidite);

        progressHumidite.setProgress((int)humidite);



        ImageView fleche = (ImageView)findViewById(R.id.imageViewFleche);

        float tourcomplet = 80;
        float diffA = 1050-pression;
        float value = tourcomplet-diffA;
        float pourcentage = value/tourcomplet;
        float anglePression = 270*pourcentage-150;
        fleche.setRotation(anglePression);


    }


}