package vcamydeb.teccart.labo4;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.widget.Toast;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

public class insert extends AppCompatActivity implements SensorEventListener {
    DatePickerDialog pickerDate;
    TimePickerDialog pickerHeure;
    EditText dateInput, heureInput, nomPointInput;
    Button btnDate,btnHeure;
    String heure,jour,pays,ville,province, nomPoint, longitude,latitude,humidite,pression,temperature;
    int gpsResult = 0;
    private  int RUN_TIME_CODE = 2 ;
    GPSLocationTracker gps;
    double longitudeDb,latitudeDb;
    private SensorManager sensorManager;
    private Sensor pressionSensor,temperatureSensor, humiditeSensor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        nomPointInput =(EditText)findViewById(R.id.editTextNomPoint);
        btnHeure=(Button)findViewById(R.id.buttonHeure);
        heureInput = (EditText)findViewById(R.id.editTextHeure);
        btnDate=(Button)findViewById(R.id.buttonDate);
        dateInput = (EditText)findViewById(R.id.editTextDate);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        pressionSensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        temperatureSensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        humiditeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);

        ///////////////////////////////////////DATEPICKER////////////////////////////////////////////////////
        dateInput.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                pickerDate = new DatePickerDialog(insert.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                dateInput.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                            }
                        }, year, month, day);
                pickerDate.show();
            }
        });
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jour = dateInput.getText().toString();
                Toast.makeText(getApplicationContext(), jour, Toast.LENGTH_LONG).show();
                dateInput.setText("Enregistré");

            }
        });


        ///////////////////////////////////TIMEPICKER////////////////////////////////////////////////////////
        heureInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldrHeure = Calendar.getInstance();
                int hour = cldrHeure.get(Calendar.HOUR_OF_DAY);
                int minutes = cldrHeure.get(Calendar.MINUTE);
                pickerHeure = new TimePickerDialog(insert.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        heureInput.setText(hourOfDay + ":" + minute);
                    }
                }, hour, minutes, true);
                pickerHeure.show();
            }
        });
        btnHeure.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                heure = heureInput.getText().toString();
                Toast.makeText(getApplicationContext(), heure, Toast.LENGTH_LONG).show();
                heureInput.setText("Enregistré");

            }
        });

        //verif permission gps
        if (isGPSAllowed()) {
//            Toast.makeText(this, "Vous avez deja les permissions pour le GPS", Toast.LENGTH_LONG).show();
            return;
        }
        requestPermissionGPS();
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    public void insertNewData(View view) {

///////////////////GPS/////////////////////////////////////////////////////////////////////////////////////////////////
        final Geocoder geocoder = new Geocoder(this);

        gps = new GPSLocationTracker(insert.this, gpsResult);
        if (gps.canGetLocation) {
            latitudeDb = gps.getLatitude();
            longitudeDb = gps.getLongitude();

            List<Address> adressList = null;
            try {
                adressList = geocoder.getFromLocation(latitudeDb, longitudeDb, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }


            pays = adressList.get(0).getCountryName();
            province = adressList.get(0).getAdminArea();
            ville = adressList.get(0).getLocality();
            nomPoint = nomPointInput.getText().toString();
            longitude = String.valueOf(longitudeDb);
            latitude = String.valueOf(latitudeDb);

//            Toast.makeText(getApplicationContext(), "Lat: "
//                    + latitude + "\nLongitude: " + longitude +"\nTemperature: "+temperature+"\nPression :"+pression+"\nhumidite :"+humidite, Toast.LENGTH_LONG).show();
        } else
        {
            gps.showSettingsAlert();
        }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    //////////////////envoi data dans base de donnée//////////////////////////////////////////////////////////////////
        String cmd = "2";

        asynctask insert = new asynctask(this);
        insert.execute(cmd,nomPoint,jour,heure,longitude,latitude,ville,province,pays,pression,temperature,humidite);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        Sensor sensor = event.sensor;
        if(sensor.getType() == Sensor.TYPE_PRESSURE){
            float pressionResult = event.values[0];
            pression = String.valueOf(pressionResult);
        }
        else if(sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE){
            float temperatureResult = event.values[0];
            temperature = String.valueOf(temperatureResult);
        }
        else if(sensor.getType() == Sensor.TYPE_RELATIVE_HUMIDITY){
            float humiditeResult = event.values[0];
            humidite = String.valueOf(humiditeResult);
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this,sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this,sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this,sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY), SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }



    ///////verif si permission gps granted////////////////////////////////////
    private boolean isGPSAllowed()
    {
        int resultGps = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if(resultGps == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    /////////////////////request permission gps/////////////////////////////
    private void requestPermissionGPS()
    {
        if(ActivityCompat.shouldShowRequestPermissionRationale(insert.this, Manifest.permission.ACCESS_FINE_LOCATION)){
            showAlertDialog();
        }
        ActivityCompat.requestPermissions(insert.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},RUN_TIME_CODE);
    }
    //////////////////alerDialog demandant l'acces au gps//////////////////////////////////////////
    private void showAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Runtime Permission");
        alertDialogBuilder.setMessage("Cette application a besoin d'accéder aux:"+"GPS et  données cellulaire."+"Donner les permissions.").setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}