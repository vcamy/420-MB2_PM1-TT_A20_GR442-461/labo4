package vcamydeb.teccart.labo4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class relevesArrayAdapter extends ArrayAdapter<releve> {


    private final Context context;
    private ArrayList<releve> liste;
    private int resource;

    public relevesArrayAdapter(@NonNull Context context, int resource, List<releve> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource = resource;
        liste = new ArrayList<releve>();
        liste = (ArrayList<releve>) objects;

    }

    //@Override
    public View getView(int position, View convertView, ViewGroup parent) {

        releve temp = this.liste.get(position);

        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        convertView = layoutInflater.inflate(this.resource, parent, false);

        TextView nom = (TextView) convertView.findViewById(R.id.lblNom);
        TextView ville = (TextView) convertView.findViewById(R.id.lblVille);
        TextView pays = (TextView) convertView.findViewById(R.id.lblPays);


        nom.setText(temp.getNom());
        ville.setText(temp.getVille());
        pays.setText(temp.getPays());


        return convertView;
    }

}
