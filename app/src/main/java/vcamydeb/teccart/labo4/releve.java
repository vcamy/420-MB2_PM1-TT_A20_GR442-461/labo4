package vcamydeb.teccart.labo4;

public class releve {

    private String id;
    private String nom;
    private String jour;
    private String heure;
    private String longitude;
    private String latitude;
    private String ville;
    private String province;
    private String pays;
    private String pression;
    private String temperature;
    private String humidite;

    public releve(String id, String nom, String jour, String heure, String longitude, String latitude, String ville, String province, String pays, String pression, String temperature, String humidite) {
        this.id = id;
        this.nom = nom;
        this.jour = jour;
        this.heure = heure;
        this.longitude = longitude;
        this.latitude = latitude;
        this.ville = ville;
        this.province = province;
        this.pays = pays;
        this.pression = pression;
        this.temperature = temperature;
        this.humidite = humidite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getJour() {
        return jour;
    }

    public void setJour(String jour) {
        this.jour = jour;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getPression() {
        return pression;
    }

    public void setPression(String pression) {
        this.pression = pression;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidite() {
        return humidite;
    }

    public void setHumidite(String humidite) {
        this.humidite = humidite;
    }
}
